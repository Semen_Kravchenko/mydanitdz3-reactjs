exports.cardsInfo = [
    { 
        id: 1, 
        title: "Blinding Lights",
        author: "The Weeknd",
        price: 14.99,
        imgSrc: "./img/blindingLights-cover.jpg",
        artNum: 100001,
        color: "#222" 
    },
    {   
        id: 2, 
        title: "Circles",
        author: "Post Malone",
        price: 12.99,
        imgSrc: "./img/circles-cover.jpg",
        artNum: 100002,
        color: "#222" 
    },
    { 
        id: 3, 
        title: "Don't Start Now",
        author: "Dua Lipa",
        price: 23.99,
        imgSrc: "./img/dontStartNow-cover.jpg",
        artNum: 100003,
        color: "#222" 
    },
    {   
        id: 4, 
        title: "The Box",
        author: "Roddy Ricch",
        price: 18.99,
        imgSrc: "./img/theBox-cover.jpg",
        artNum: 100004,
        color: "#222" 
    },        { 
        id: 5, 
        title: "Rockstar",
        author: "DaBaby",
        price: 15.99,
        imgSrc: "./img/rockstar-cover.jpg",
        artNum: 100005,
        color: "#222" 
    },
    {   
        id: 6, 
        title: "Adore You",
        author: "Harry Styles",
        price: 8.99,
        imgSrc: "./img/adoreYou-cover.jpg",
        artNum: 100006,
        color: "#222" 
    },        { 
        id: 7, 
        title: "Life Is Good",
        author: "Future",
        price: 25.99,
        imgSrc: "./img/lifeIsGood-cover.jpg",
        artNum: 100007,
        color: "#222" 
    },
    {   
        id: 8, 
        title: "Memories",
        author: "Maroon 5",
        price: 17.99,
        imgSrc: "./img/memories-cover.jpg",
        artNum: 100008,
        color: "#222" 
    },        
    { 
        id: 9, 
        title: "The Bones",
        author: "Maren Morris",
        price: 9.99,
        imgSrc: "./img/theBones-cover.jpg",
        artNum: 100009,
        color: "#222" 
    },
    {   
        id: 10, 
        title: "Someone You Loved",
        author: "Lewis Capaldi",
        price: 13.99,
        imgSrc: "./img/someoneYouLoved-cover.jpg",
        artNum: 100010,
        color: "#222" 
    }
]