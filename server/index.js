const express = require('express');
const db = require('./db/db');
const app = express();


app.get('/api/cards', (req, res) => {
  res.send(db.cardsInfo);
})

app.get('/api/cards/:cardId', (req, res) => {
  const { cardId } = req.params
  res.send(db.cardsInfo.find(e => e.id === +cardId));
})

const port = 8080;

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})