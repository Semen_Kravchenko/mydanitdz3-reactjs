import { useState, useEffect } from 'react';
import axios from 'axios';
import './App.scss';
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import Body from './components/Body/Body';
import Modal from './components/Modal/Modal';
import AppRoutes from './routes/AppRoutes';


const App = () => {
  const initialCardStorage = (key) => JSON.parse(localStorage.getItem(key)) || [];

  const [cardsInfo, setCardsInfo] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [addedToCart, setAddedToCart] = useState(initialCardStorage("addedToCart"));
  const [addedToWishList, setAddedToWishList] = useState(initialCardStorage("addedToWishList"));
  const [isRm, setIsRm] = useState(false);

  const [activeModal, setActiveModal] = useState([false, [], '']);

  const addToCart = (artNum) => {
    const getCardByArtNum = cardsInfo.filter(e => e.artNum === artNum);
    if(addedToCart.includes(...getCardByArtNum)) return;
    setAddedToCart([...addedToCart, ...getCardByArtNum]);
    setActiveModal([!activeModal[0], getCardByArtNum, 'Added To Cart']);
  }

  const addToWishList = (artNum) => {
    const getCardByArtNum = cardsInfo.filter(e => e.artNum === artNum);
    if(addedToWishList.includes(...getCardByArtNum)) return;
    setAddedToWishList([...addedToWishList, ...getCardByArtNum]);
  }

  const removeAddedCard = (artNum, currentRef) => {
    setIsRm(!isRm);
    if(currentRef === 'addedToCart') setAddedToCart(() => {
      return addedToCart.filter(e => e.artNum !== artNum);
    });

    if(currentRef === 'addedToWishList') setAddedToWishList(() => {
      localStorage.setItem(`fill${artNum}`, JSON.stringify(''))
      return addedToWishList.filter(e => e.artNum !== artNum);
    });
  } 

  const getCardsInfo = () => {
    axios.get('/api/cards')
      .then(res => {
        setCardsInfo(res.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000); 
      })
  }

  useEffect(() => {
    getCardsInfo();
  }, []);

  useEffect(() => {
    if(isRm) setIsRm(!isRm)
    localStorage.setItem('addedToCart', JSON.stringify(addedToCart));
  }, [addedToCart, isRm]);

  useEffect(() => {
    if(isRm) setIsRm(!isRm)
      localStorage.setItem('addedToWishList', JSON.stringify(addedToWishList));
  }, [addedToWishList, isRm]);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <Header />
      <Body>
        <AppRoutes cardsInfo={cardsInfo} isLoading={isLoading} addToCart={addToCart} addToWishList={addToWishList} removeAddedCard={removeAddedCard} />
      </Body>
      {activeModal[0] && <Modal setActiveModal={setActiveModal} activeModal={activeModal[0]} content={activeModal[1]} heading={activeModal[2]} />}
    </>
  );
}

export default App;