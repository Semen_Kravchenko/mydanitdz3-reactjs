import React from 'react';
import PropTypes from 'prop-types';
import './Cart.scss';
import AddedCards from '../../components/AddedCards/AddedCards';
import EmptyPage from '../../components/EmptyPage/EmptyPage';

const Cart = ({removeAddedCard}) => {
    const cartRef = 'addedToCart';
    const showCards = (arrInfo) => {
        if(arrInfo.length === 0) return <EmptyPage target={'cart'} />
        return arrInfo.map(e => <AddedCards key={e.artNum} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} removeAddedCard={removeAddedCard} currentRef={cartRef}  />);
    }
    const subtotalFn = (arrInfo) => {
        if(arrInfo.length === 0) return 0;
        const priceArr = arrInfo.map(e => e.price);
        const totalPrice = priceArr.reduce((x, y) => x + y)
        return totalPrice.toFixed(2);
    }
    return (
        <div className='cart__wrapper'>
            <h2>Shopping Cart</h2>
            {showCards(JSON.parse(localStorage.getItem('addedToCart')))}
            <div className='cart__footer'>
                <div className='container'>
                    <div className='cart__footer-inner'>
                        <p className='cart__total'>Subtotal: ${subtotalFn(JSON.parse(localStorage.getItem('addedToCart')))}</p>
                        <button className='cart__checkout'>Checkout</button>
                    </div>
                </div>
            </div>
        </div>
    )  
}

Cart.propTypes = {
    removeAddedCard: PropTypes.func.isRequired
}

export default Cart;