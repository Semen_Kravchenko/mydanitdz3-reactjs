import React, { PureComponent } from 'react';
import './Error500.scss';

class Error500 extends PureComponent {
  render() {
    return (
      <div className='error-500'>
        <h3>An error has occurred</h3>
        <h4>But we already sent droids to fix it</h4>
      </div>
    )
  }
}

export default Error500