import React from 'react';
import './Error404.scss';

const Error404 = () => {
    return (
        <div className='error404'>
            <h2>404</h2>
            <h3>Page not found</h3>
        </div>
    );
}

export default Error404;