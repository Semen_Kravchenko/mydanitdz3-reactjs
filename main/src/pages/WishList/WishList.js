import React from 'react';
import PropTypes from 'prop-types';
import AddedCards from '../../components/AddedCards/AddedCards';
import EmptyPage from '../../components/EmptyPage/EmptyPage';

const WishList = ({ removeAddedCard  }) => {
    const cartRef = 'addedToWishList';
    const showCards = (arrInfo) => {
        if(arrInfo.length === 0) return <EmptyPage target={'wish list'} />
        return arrInfo.map(e => <AddedCards key={e.artNum} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} removeAddedCard={removeAddedCard} currentRef={cartRef} />);
    }

    return (
        <div className='wishList__wrapper'>
            <h2>Wish List</h2>
            {showCards(JSON.parse(localStorage.getItem('addedToWishList')))}
        </div>
    )  
}

WishList.propTypes = {
    removeAddedCard: PropTypes.func.isRequired
}

export default WishList;