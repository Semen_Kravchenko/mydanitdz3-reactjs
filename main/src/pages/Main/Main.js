import React from 'react';
import './Main.scss';
import PropTypes from "prop-types";
import Cards from '../../components/Cards/Cards';

const Main = ({ cardsInfo, addToCart, addToWishList }) => {
    const showCards = (arrInfo) => {
        return arrInfo.map(e => <Cards key={e.artNum} id={e.id} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} addToCart={addToCart} addToWishList={addToWishList} />);
    }
    return (
        <div className='cards__wrapper'>
            {showCards(cardsInfo)}
        </div>
    )  
}

Main.propTypes = {
    cardsInfo: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToWishList: PropTypes.func.isRequired,
}

export default Main;