import React from 'react';
import PropTypes from 'prop-types';
import Cards from '../Cards/Cards';
import "./Modal.scss";

const Modal = ({ content, activeModal, setActiveModal, heading }) => {
    const showCards = (arrInfo) => {
        return arrInfo.map(e => <Cards key={e.artNum} id={e.id} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} hideBtn={true} />);
    }
    return (
        <div className="modal" onClick={() => setActiveModal(!activeModal[0])}>
            <div className="modal__content" onClick={(e) => e.stopPropagation()}>
                <div className="modal__header">
                    <p className="modal__header-title">{heading}</p>
                    <button className="modal__close" onClick={() => setActiveModal(!activeModal[0])}>{String.fromCharCode(0x2716)}</button>
                </div>
                <div className="modal__body">{showCards(content)}</div>
                <div className="modal__footer">
                    <button className='modal__footer-btn' onClick={() => setActiveModal(!activeModal[0])}>
                        ok
                    </button>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    content: PropTypes.array.isRequired,
    activeModal: PropTypes.bool.isRequired,
    setActiveModal: PropTypes.func.isRequired, 
    heading: PropTypes.string.isRequired
}

export default Modal;