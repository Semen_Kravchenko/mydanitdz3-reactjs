import React, { PureComponent } from 'react';
import Error500 from '../../pages/Error500/Error500';


class ErrorBoundary extends PureComponent {
    state = {
        errorPresent: false
    }

    static getDerivedStateFromError(error) {
        return {errorPresent: true}
    }

    render() {
        const { errorPresent } = this.state;
        const { children } = this.props;

        if (errorPresent) {
            return <Error500 />
        }
        
        return children;
    }
}

export default ErrorBoundary;