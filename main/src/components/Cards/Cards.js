import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import StarIcon from '../../theme/icons/StarIcon';
import './Cards.scss';

const Cards = ({ title, author, price, imgSource, artNum, color, addToCart, addToWishList, hideBtn }) => {    
    const initialFillStorage = (artNum) => JSON.parse(localStorage.getItem(`fill${artNum}`)) || '';
    const [isFilled, setIsFilled] = useState(initialFillStorage(artNum));

    const handlerClick = () => {
        if(isFilled === '') setIsFilled('gold')
        addToWishList(artNum);
    }

    useEffect(() => {
        localStorage.setItem(`fill${artNum}`, JSON.stringify(isFilled));
    }, [isFilled, artNum]);

    return (
        <div className="cards" style={{color}}>
            <img className="cards__img" src={imgSource} alt={`card-img-#${artNum}`} /> 
            {hideBtn || <div className='cards__wish'>
                <StarIcon className='cards__wish-icon' fill={isFilled} />
                <div className='cards__wish-overlay' onClick={handlerClick} />
            </div>}
            <div className="cards__text-wrapper">
                <p className="cards__title">{title}</p>
                <p className="cards__author">by {author}</p>
                <p className="cards__artNum">art#{artNum}</p>
                <div className="cards__footer">
                    <p className="cards__price">${price}</p>
                    {hideBtn || <button className={`cards__buy ${artNum}`} onClick={() => addToCart(artNum)}>add to cart</button>}
                </div>
            </div>
        </div>
    );
}

Cards.defaultProps = {
    hideBtn: false,
}

Cards.propTypes = {
    title: PropTypes.string.isRequired, 
    author: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    imgSource: PropTypes.string.isRequired, 
    artNum: PropTypes.number.isRequired, 
    color: PropTypes.string.isRequired, 
    addToCart: PropTypes.func, 
    addToWishList: PropTypes.func
}

export default Cards;