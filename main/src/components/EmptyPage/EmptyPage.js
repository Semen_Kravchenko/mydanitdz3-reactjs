import React from 'react';
import PropTypes from 'prop-types';
import './EmptyPage.scss';

const EmptyPage = ({ target }) => {
    return (
        <div className='empty-page'>
            <p className='empty-page__text'>Your {target} is empty</p>
        </div>
    );
}

EmptyPage.propTypes = {
    target: PropTypes.string.isRequired
}

export default EmptyPage;