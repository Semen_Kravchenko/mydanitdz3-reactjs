import React from 'react';
import './Header.scss';
import HeaderLogoIcon from '../../theme/icons/HeaderLogoIcon';
import HeaderCartIcon from '../../theme/icons/HeaderCartIcon';
import StarIcon from '../../theme/icons/StarIcon';
import { Link } from 'react-router-dom';

const Header = () => {
        return (
            <header className="page-header header">
                <div className="container">
                    <div className="header__inner">
                        <Link to='/main' className="header__logo">
                            <HeaderLogoIcon className='header__logo-icon' /> charts
                        </Link>
                        <div className="header__buttons">
                            <Link to='/wishList' className="header__wishList">
                                <StarIcon className="header__wishList-icon" fill='#ffc107' />
                            </Link>
                            <Link to='/cart' className="header__cart">
                                <HeaderCartIcon className='header__cart-icon' />
                            </Link>
                        </div>
                    </div>
                </div>
            </header>
        );
}

export default Header;