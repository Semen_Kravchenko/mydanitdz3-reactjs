import React from 'react';
import './AddedCards.scss';
import PropTypes from 'prop-types';

const AddedCards = ({ title, author, price, imgSource, artNum, color, removeAddedCard, currentRef  }) => {
    return (
        <div className={`added-card card#${artNum}`} style={{color}}>
            <div className='added-card__img-wrapper'>
                <img className='added-card__img' src={imgSource} alt={`added-card#${artNum}`} />
            </div>
            <div className='added-card__content-wrapper'>
                <div className='added-card__top-block'>
                    <p className="added-card__full-title">
                        <span className='added-card__title'>{title}</span> by <span className='added-card__author'>{author}</span>
                    </p>
                    <button className='added-card__rm' onClick={() => removeAddedCard(artNum, currentRef)}>{String.fromCharCode(0x2716)}</button>
                </div>
                <div className='added-card__bottom-block'>
                    <p className='added-card__artNum'>art#{artNum}</p>
                    <p className='added-card__price'>${price}</p>
                </div>
            </div>
        </div>
    );
}

AddedCards.propTypes = {
    title: PropTypes.string.isRequired, 
    author: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    imgSource: PropTypes.string.isRequired, 
    artNum: PropTypes.number.isRequired, 
    color: PropTypes.string.isRequired, 
    removeAddedCard: PropTypes.func.isRequired, 
    currentRef: PropTypes.string.isRequired
}

export default AddedCards;