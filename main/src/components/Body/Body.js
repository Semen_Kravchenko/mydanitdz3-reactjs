import React from 'react';
import PropTypes from 'prop-types';

const Body = ({ children }) => {
    return (
        <section className="content">
            <div className="container">
                <div className="content__inner">
                    {children}
                </div>
            </div>
        </section>
    );
}

Body.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Body;