import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';
import Cart from '../pages/Cart/Cart';
import Error404 from '../pages/Error404/Error404';
import Main from '../pages/Main/Main';
import WishList from '../pages/WishList/WishList';

const AppRoutes = ({ cardsInfo, isLoading, addToCart, addToWishList, removeAddedCard }) => {
    return (
        <Switch>
            <Redirect exact from='/' to='/main' />
            <Route exact path='/main'>{isLoading || <Main cardsInfo={cardsInfo} addToCart={addToCart} addToWishList={addToWishList} />}</Route>
            <Route exact path='/cart'><Cart removeAddedCard={removeAddedCard}   /></Route> 
            <Route exact path='/wishList'><WishList removeAddedCard={removeAddedCard}  /></Route>
            <Route path='*'><Error404 /></Route>
        </Switch>
    );
};

AppRoutes.propTypes = {
    cardsInfo: PropTypes.array.isRequired, 
    isLoading: PropTypes.bool.isRequired, 
    addToCart: PropTypes.func.isRequired, 
    addToWishList: PropTypes.func.isRequired, 
    removeAddedCard: PropTypes.func.isRequired
}

export default AppRoutes;
