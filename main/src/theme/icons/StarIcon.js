import React from 'react';
import PropTypes from 'prop-types';

const StarIcon = ({ className, fill, stroke }) => {
    return (
        <svg className={className} xmlns="http://www.w3.org/2000/svg" width='40' viewBox="-1.5 -1.5 27 27" fill={fill} stroke={stroke}>
            <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
        </svg>
    )
}

StarIcon.propTypes = {
    className: PropTypes.string.isRequired,
    fill: PropTypes.string.isRequired,
    stroke: PropTypes.string.isRequired
}

StarIcon.defaultProps = {
    fill: '#222',
    stroke: '#ffc107',
}

export default StarIcon;